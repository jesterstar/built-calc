import { createAction, Action } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import { Observable, of } from 'rxjs';
import {concatMap} from 'rxjs/operators';

export const INIT_APP = 'INIT_APP';
export const INIT_APP_SUCCESS = 'INIT_APP_SUCCESS';

export const initApp = createAction(INIT_APP);
export const initAppSuccess = createAction<object>(INIT_APP_SUCCESS);

export const initAppEpic = (action$: Observable<Action>) => {
    return action$.pipe(
        ofType(INIT_APP),
        concatMap((): any => {
            console.log('here');
            return of(initAppSuccess({ loaded: true }));
        })
    );
}