import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { INIT_APP_SUCCESS } from './app-init.service';

const initAppInitialState = {
    loaded: false
};

interface InitAppInterface {
    loaded: boolean
}

const initAppSuccess = (initAppState: any, action: PayloadAction<InitAppInterface>) => {
    console.log('App init success');
    return { ...initAppState, ...action.payload };
};

const actionsMap = {
    [INIT_APP_SUCCESS]: initAppSuccess
};

export const initAppReducer = createReducer(initAppInitialState, actionsMap);