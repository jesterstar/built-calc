import { combineReducers } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';

import history from './helpers/history';
import { initAppReducer } from './services/app-init/app-init.reducer';

const rootReducer = combineReducers({
    appInit: initAppReducer,
    router: connectRouter(history) // router should be last reducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;