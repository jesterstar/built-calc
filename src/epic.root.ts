import { combineEpics } from 'redux-observable';

// init-app service
import { initAppEpic } from './services/app-init/app-init.service';

const rootEpic = combineEpics(
    initAppEpic
);

export default rootEpic;