import * as R from 'ramda';
import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './reducer.root';
import rootEpic from './epic.root';
import history from './helpers/history';

let store: any = null;

const initialState = {};
// @ts-ignore
const composeEnhancers = composeWithDevTools({
    maxAge: 100, // store 100 last actions in history
    // trace: true,
    serialize: {
    // @ts-ignore
        replacer: (key, value) =>
            R.cond([
                [R.isNil, v => v],
                [R.is(Blob), () => '<<Blob>>'],
                [R.is(HTMLElement), () => '<<HTMLElement>>'],
                [R.T, v => v]
            ])(value)
    }
});
const dependencies = { store };

const epicMiddleware = createEpicMiddleware({ dependencies });

store = createStore(
    rootReducer,
    initialState,
    // @ts-ignore
    composeEnhancers(applyMiddleware(routerMiddleware(history), epicMiddleware))
);

dependencies.store = store;
// @ts-ignore
epicMiddleware.run(rootEpic);

let prevState = {};
const keysToFilter = ['lastAction', 'prevState'];
store.subscribe(() => {
    const state = store.getState();
    const meaningfulState = R.omit(keysToFilter, state);

    if (!R.equals(meaningfulState, prevState)) {
        prevState = meaningfulState;
        store.dispatch({ type: 'SET_PREV_STATE', payload: meaningfulState });
    }
});

// TODO maybe handle it in other way
// @ts-ignore
window.dispatch = (...args) => store.dispatch(...args);

export default store;
