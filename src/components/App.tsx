import * as R from 'ramda';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { createSelectorCreator, createStructuredSelector, defaultMemoize } from 'reselect';
import { WithStyles, withStyles } from '@material-ui/core';

import { initApp } from '../services/app-init/app-init.service';
import logo from '../assets/logo.svg';
import './App.css';

interface IStateProps {}

interface IDispatchProps {
    initApp(): void
}
type Props = IStateProps & IDispatchProps & WithStyles;

const App: React.FC<Props> = ({ initApp, classes }) => {
    useEffect(() => {
        initApp();
    }, [])

    return (
        <>
            <div className={classes.root}>test</div>
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.tsx</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </div>
        </>
    );
}

export default R.compose<any, any>(
    withStyles({
        root: {
            color: 'black'
        }
    }),
// @ts-ignore
    connect(() => ({}), { initApp })
// @ts-ignore
)(App);

